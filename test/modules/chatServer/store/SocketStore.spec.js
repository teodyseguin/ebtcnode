var SocketStore = require('../../../../app/modules/chatServer/store/SocketStore');
var expect      = require('chai').expect;

describe('chatServer: SocketStore should have exposed properties', function() {
  it('connections', function() {
    SocketStore.should.have.property('connections');
    expect(SocketStore.connections).to.be.a('array');  
  });
});

