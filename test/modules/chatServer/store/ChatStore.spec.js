'use strict';

var ChatStore = require('../../../../app/modules/chatServer/store/ChatStore');
var expect    = require('chai').expect;

describe('chatServer: ChatStore should have exposed properties', function() {
  it('banList', function() {
    ChatStore.should.have.property('banList');
    expect(ChatStore.banList).to.be.a('array');  
  });

  it('messageList', function() {
    ChatStore.should.have.property('messageList'); 
    expect(ChatStore.messageList).to.be.a('array');
  });
});

