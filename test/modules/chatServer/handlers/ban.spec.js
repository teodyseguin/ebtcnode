'use strict';

var ban       = require('../../../../app/modules/chatServer/handlers/ban');
var ChatStore = require('../../../../app/modules/chatServer/store/ChatStore');
var expect    = require('chai').expect;
var sinon     = require('sinon');

describe('chatServer: ban handler', function() {
  it('chatServer: ban handler should have a method bans()', function() {
    ban.should.have.property('bans'); 
  });

  it('chatServer: ChatStore.banList should be zero in length', function() {
    expect(ChatStore.banList).to.be.empty; 
  });

  it('chatServer: when ban.bans() is called, ChatStore.banList should be populated', function() {
    var mockRequest = {
      body: { params: { uid: 1 } },
      method: 'POST'
    };
    var mockResponse = {};
    ban.bans(mockRequest, mockResponse);
    expect(ChatStore.banList).to.have.lengthOf(1);
  });

  it('chatServer: when ban.bans() is called, should be able to make a GET request', function(done) {
    var mockRequest = {
      body: { params: {} },
      method: 'GET'
    }; 
    var mockResponse = {
      writeHead: sinon.spy(),
      write: sinon.spy(),
      end: sinon.spy()
    };
    ban.bans(mockRequest, mockResponse, function(result) {
      expect(result.error).to.be.false;
      done();
    });
  });
});

