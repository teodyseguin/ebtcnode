'use strict';

var message   = require('../../../../app/modules/chatServer/handlers/message');
var ChatStore = require('../../../../app/modules/chatServer/store/ChatStore');
var expect    = require('chai').expect;
var sinon     = require('sinon');
var should    = require('should');

describe('chatServer: message handler', function() {
  it('chatServer: message handler should have a method messages()', function() {
    message.should.have.property('messages'); 
  });

  it('chatServer: ChatStore.messageList should be zero in length', function() {
    expect(ChatStore.messageList).to.be.empty; 
  });

  it('chatServer: when message.messages() is called, ChatStore.messageList should be populated', function(done) {
    var mockRequest = {
      header: sinon.spy(),
      body: { params: {} },
      connection: sinon.spy(),
      method: 'POST'
    };
    var mockResponse = {
      writeHead: sinon.spy(),
      write: sinon.spy(),
      end: sinon.spy()
    };
    message.messages(mockRequest, mockResponse);
    expect(ChatStore.messageList).to.be.instanceof(Array);
    done();
  });

  it('chatServer: when message.messages() is called, should add ip_address property if none is present', function(done) {
    var mockRequest = {
      header: sinon.spy(),
      body: { params: {} },
      connection: sinon.spy(),
      method: 'GET'
    }; 
    var mockResponse = {
      writeHead: sinon.spy(),
      write: sinon.spy(),
      end: sinon.spy()
    };
    message.messages(mockRequest, mockResponse, function(result) {
      expect(result.error).to.be.false;
      done();  
    });
  });
});

