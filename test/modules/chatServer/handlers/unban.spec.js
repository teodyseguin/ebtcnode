'use strict';

var unban     = require('../../../../app/modules/chatServer/handlers/unban');
var ChatStore = require('../../../../app/modules/chatServer/store/ChatStore');
var expect    = require('chai').expect;
var sinon     = require('sinon');
var should    = require('should');

describe('chatServer: unban handler', function() {
  it('chatServer: unban handler should have a method unban()', function() {
    unban.should.have.property('unban'); 
  });

  it('chatServer: pushing a mock uid(1) to ChatStore.banList', function() {
    ChatStore.banList.push(1);
    ChatStore.banList.indexOf(1).should.equal(0); 
  });

  it('chatServer: unban() should be able to locate uid(1) and remove it', function() {
    var mockRequest = {
      header: sinon.spy(),
      body: { params: { uid: 1 } },
      connection: sinon.spy(),
      method: 'POST'
    };
    var mockResponse = {
      writeHead: sinon.spy(),
      write: sinon.spy(),
      end: sinon.spy()
    };

    unban.unban(mockRequest, mockResponse);
    var found = ChatStore.banList.indexOf(1);
    ChatStore.banList.splice(found, 1);
    ChatStore.banList.indexOf(1).should.equal(-1);
  });
});

