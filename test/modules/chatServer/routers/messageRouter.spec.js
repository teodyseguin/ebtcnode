'use strict';

var messageRouter = require('../../../../app/modules/chatServer/routers/messageRouter');
var expect        = require('chai').expect;

describe('chatServer: messageRouter()', function() {
  it('messageRouter() should be a router function', function() {
    expect(messageRouter).to.be.an('function'); 
  });
});

