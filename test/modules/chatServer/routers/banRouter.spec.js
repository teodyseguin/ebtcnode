'use strict';

var banRouter = require('../../../../app/modules/chatServer/routers/banRouter');
var expect    = require('chai').expect;

describe('chatServer: banRouter()', function() {
  it('banRouter() should be a router function', function() {
    expect(banRouter).to.be.an('function'); 
  });
});

