'use strict';

var unbanRouter = require('../../../../app/modules/chatServer/routers/unbanRouter');
var expect      = require('chai').expect;

describe('chatServer: unbanRouter()', function() {
  it('unbanRouter() should be a router function', function() {
    expect(unbanRouter).to.be.an('function');
  });
});

