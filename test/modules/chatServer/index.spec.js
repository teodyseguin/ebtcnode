'use strict';

var chatServer = require('../../../app/modules/chatServer');
var should     = require('should');

describe('chatServer: should have exposed methods', function() {
  it('addClient()', function() {
    chatServer.should.have.property('addClient');
  });

  it('banRouter()', function() {
    chatServer.should.have.property('banRouter'); 
  });

  it('messageRouter()', function() {
    chatServer.should.have.property('messageRouter');
  });

  it('unbanRouter()', function() {
    chatServer.should.have.property('unbanRouter');
  });
});

