'use strict';

var config  = require('../../../app/config')[process.env.NODE_ENV || 'dev'];
var expect  = require('chai').expect;
var Handler = require('../../../app/modules/core/Handler');
var http    = require('http');
var should  = require('should');

describe('core: Handler class', function() {
  var handler = new Handler(null, 'POST', config, 'chatLog', http);

  it('Should be able to create an object instance of Handler', function() {
    expect(handler).to.be.an.instanceof(Handler);
  });

  it('Should have a public property props', function() {
    handler.should.have.property('props');
  });

  it('Should have a public method execute()', function() {
    handler.should.have.property('execute');
  });

  it('Should have the property props populated', function() {
    expect(handler).to.be.instanceof(Object);
  });
});

