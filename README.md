# Welcome to EZBTC Node Server

## Pre-Requisite Tools
Need to have these tools in your local to run this repo

- NodeJS - https://nodejs.org
- NPM - https://www.npmjs.com

## How to Run
- Open your terminal and change directory to the project root
- Run `npm install` to download the project denpendencies
- Finally, run `npm start`

## How to Run Test
I had Mocha installed, as the Testing framework. To run it
- Run the command `npm test` from the project root directory

## How to Stop
- Go to the project root folder and run `npm stop`

## How to view logs
- First, you must have `forever` module, installed globally: `npm install forever -g`
- Once that is installed, use the command `forever list`. It should print out the current running Forever processes
- From there, you should see the `logfile`
- `tail -f <logfile/path/filename>` to continously view the log's tail as it grow
- You may use `cat <logfile/path/filename>` to print the whole log from the screen OR use any editor to open up the log file

