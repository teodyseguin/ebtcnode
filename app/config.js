module.exports = {
  dev: {
    host: {
      name: 'localhost',
      port: 32769,
      api: {
        chatLog: '/js/chat_log',
        chatBan: '/js/chat_bans',
        marketTrends: '/js/market_trends',
        transactions: '/js/transactions',
        userData: '/js/user',
        chartData: '/js/chart_data',
        altCoinsData: '/js/altcoins_data'
      },
      cron: {
        chartData: '5 * * * * *',
        altCoinsData: '3 * * * * *'
      }
    },
    mysql: {
      host: 'localhost',
      database: 'ezbtcdrupal',
      username: '',
      password: '',
      port: 3306,
    }
  },
  test: {
    host: {
      name: 'drupal.ezstaging.co',
      port: 80,
      api: {
        chatLog: '/js/chat_log',
        chatBan: '/js/chat_bans',
        marketTrends: '/js/market_trends',
        transactions: '/js/transactions',
        userData: '/js/user',
        chartData: '/js/chart_data',
        altCoinsData: '/js/altcoins_data'
      },
      cron: {
        chartData: '30 * * * * *',
        altCoinsData: '3 * * * * *'
      }
    },
    mysql: {
      host: '',
      database: '',
      username: '',
      password: '',
      port: 0000,
    }
  },
  prod: {
    host: {
      name: 'drupal.ezstaging.co',
      port: 80,
      api: {
        chatLog: '/js/chat_log',
        chatBan: '/js/chat_bans',
        marketTrends: '/js/market_trends',
        transactions: '/js/transactions',
        userData: '/js/user',
        chartData: '/js/chart_data',
        altCoinsData: '/js/altcoins_data'
      },
      cron: {
        chartData: '30 * * * * *',
        altCoinsData: '3 * * * * *'
      }
    },
    mysql: {
      host: '',
      database: '',
      username: '',
      password: '',
      port: 0000,
    }
  }
};

