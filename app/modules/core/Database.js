'use strict';

var Sequelize  = require('sequelize');
var connection = null;
var logger     = require('../../utilities/logger');

/**
 * Connect to MySQL
 *
 * @param options
 *   an object literal containing details for connecting to the Database
 *
 * @return
 *   returns a reference connection pool
 */
function connect(options) {
  connection = new Sequelize(options.database, options.username, options.password, {
    host: options.host,
    dialect: 'mysql',
    pool: {
      max: 10,
      min: 2,
      idle: 1500
    },
    logging: false
  });

  return connection;
}

/**
 * Retrieve the cached connection
 *
 * @return false || connection
 *   return a false if connection is not initiated yet
 *   or, a cached connection made by mysql connect
 */
function getConnection() {
  if (connection === null) {
    logger('No connection has been initialize yet from the Database', 'warn');
    logger('Use the connect() function to get connected to the Database', 'warn');

    return false;
  }

  logger('Returning the cached connection from the Database', 'info');

  return connection;
}

/**
 * Insert/Update a record to MySQL
 *
 * @param model
 *   the definition of the table to be created
 * @param data
 *   the data to be stored to the table
 * @param sync
 *   boolean value, either true or false
 */
function upsert(model, data, sync) {
  _sync(model, sync).then(function() {
    model.upsert(data).then(function() {
      logger('Successful in upserting a socket with uid ' + data.uid + ' into MySQL', 'info');
    })
    .catch(function(err) {
      logger('Something went wrong while upserting a socket with uid ' + data.uid + ' into MySQL', 'error');
      logger(err, 'debug')
    });
  });
}

function query(model, query, sync) {
  return _sync(model, sync).then(function() {
    return model.findAll(query);
  });
}

/**
 * Private function to force sync table creation or not
 *
 * @param model
 *   the definition of the table to be created
 * @param force
 *   boolean. either true or false
 *
 * @return
 *   {}
 */
function _sync(model, force) {
  force = force ? force : false;

  return model.sync({force: force});
}

module.exports = {
  connect,
  upsert,
  getConnection,
  query,
};

