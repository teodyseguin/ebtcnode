/**
 * A class for managing the request to Drupal endpoints
 */
function Handler(data, method, config, apiName, http) {
  this.props = {
    data    : data,
    method  : method,
    config  : config,
    apiName : apiName,
    http    : http,
    headers : null,
    options : null
  };

  this.props.headers = {
    'Content-Type'  : 'application/x-www-form-urlencoded',
    'Content-Length': this.props.data ? this.props.data.length : 0 
  };
  this.props.options = {
    host   : this.props.config.host.name,
    port   : this.props.config.host.port,
    method : this.props.method,
    path   : this.props.config.host.api[this.props.apiName],
    headers: this.props.headers
  };
}

Handler.prototype.execute = function(callback) {
  var request = this.props.http.request(this.props.options, function(response) {
    response.setEncoding('utf-8');

    var responseString = '';
    response.on('data', function(data) {
      responseString += data;
    });

    response.on('end', function() {
      try {
        var resultObject = JSON.parse(responseString);
        return callback(null, resultObject);
      }
      catch(e) {
        return callback(e); 
      }
    });
  }); 

  request.on('error', function(e) {
    return callback(e); 
  });

  if (this.props.data && this.props.options.method === 'POST') {
    request.write(this.props.data); 
  }

  request.end();
}

module.exports = Handler;

