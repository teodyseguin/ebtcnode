'use strict';

var _                      = require('lodash');
var config                 = require('../../../config')[process.env.NODE_ENV || 'dev'];
var http                   = require('http');
var TickerInformationStore = require('../store/TickerInformationStore');
var TransactionStore       = require('../store/TransactionStore');
var UserDataStore          = require('../store/UserDataStore');
var SocketStore            = require('../../chatServer/store/SocketStore');
var logger                 = require('../../../utilities/logger');
var printResponse          = require('../../../utilities/printResponse');
var Handler                = require('../../core/Handler');
var querystring            = require('querystring');

/**
 * Response handler for when the transaction endpoint is called
 */
function transactions(request, response) {
  logger('called transactions from drupal...');
  switch (request.method) {
    case 'POST':
    logger('recieved an update from Drupal');
      updateTransactionList(TransactionStore, request.body.params);
      response.end();

      var handler = new Handler(null, 'GET', config, 'marketTrends', http);
      handler.execute(function(error, result) {
        if (error) {
          logger('Something went wrong');
          logger(error);
          return;
        }
        
        updateTickerInformation(TickerInformationStore, result.data);
        sendMarketTickerEvent(SocketStore, result.data);
        sendTransactionDataEvent(SocketStore, request.body.params);
        sendUserDataEvent(SocketStore, result.data, Handler, config, http);

      });

      break;
  }
}

/**
 * Push the latest transaction object to the Transaction store
 *
 * @param TransactionStore
 *   the store that keeps all the transaction objects
 * @param data
 *   the request data being sent from drupal
 */
function updateTransactionList(TransactionStore, data) {
  logger('Updating the transactions', 'info');

  TransactionStore.transactionList.push(data);
}

/**
 * Push the latest ticker objec to the ticker information store
 *
 * @param TickerInformationStore
 *   the store that keeps all ticker information objects
 */
function updateTickerInformation(TickerInformationStore, data) {
  logger('Updating ticker information store list', 'info');

  TickerInformationStore.tickerInformationList.push(data);
}

/**
 * Send a socket event market_ticker
 *
 * @param socketStore
 *   the store object containing connected sockets
 *   connected sockets are acquired when someone connects in drupal
 * @param data
 *   the data result, returned from calling market trends drupal endpoint
 */
function sendMarketTickerEvent(socketStore, data) {
  // we broadcast to all connected sockets
  logger('Broadcast the market ticker updates', 'info');

  for (var i = 0; i < socketStore.connections.length; i++) {
    socketStore.connections[i].socket.emit('market_ticker', data);
  }
}

/**
 * Send a socket event transaction_data
 *
 * @param socketStore
 *   the store object containing connected sockets
 * @param data
 *   the request data being sent by drupal to node server transaction endpoint
 */
function sendTransactionDataEvent(socketStore, data) {
  logger('Broadcast the transaction data updates', 'info');

  for (var i = 0; i < socketStore.connections.length; i++) {
    socketStore.connections[i].socket.emit('transaction_data', data);

    socketStore.connections[i].socket.emit('transaction_update', data);

  }
}

/**
 * Send a socket event user_data
 *
 * @param socketStore
 *   the store object containing connected sockets
 * @param data
 *   the data result, returned from calling the user_data drupal endpoint
 */
function sendUserDataEvent(SocketStore, data, Handler, config, http) {
  logger('Sending updated data per connected users', 'info');

  SocketStore.connections.forEach(function(key, currentIndex) {
    var uid = SocketStore.connections[currentIndex].uid;
    var someData = querystring.stringify({ uid: uid });
    var handler = new Handler(someData, 'POST', config, 'userData', http);

    handler.execute(function(error, result) {
      if (error) {
        logger('Something went wrong while sending a request for userData', 'error');
        logger(error);

        return;
      }

      var found = _.findIndex(UserDataStore.userDataList, { 'uid': uid });

      if (found < 0) {
        UserDataStore.userDataList.push({
          uid: uid,
          data: result.data
        });
      }
      else {
        UserDataStore.userDataList[found].data = result.data;
      }

      SocketStore.connections[currentIndex].socket.emit('user_data', result.data);
    });
  });
}

module.exports = {
  transactions: transactions
};
