'use strict';

var router = require('express').Router();
var transaction = require('../handlers/transaction');

router.post('/', transaction.transactions);

module.exports = router;

