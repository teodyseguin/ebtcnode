'use strict';

var userDataList = [];
var dbService = require('../../core/Database');
var logger = require('../../../utilities/logger');
var Sequelize = require('sequelize');

var balance = 'ezbtc_balance';

var balanceSchema = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
 type: {
   type: Sequelize.STRING,
 },
 uuid: {
   type: Sequelize.STRING,
 },
 amount__number: {
   type: Sequelize.FLOAT,
 },
 amount__currency_code: {
   type: Sequelize.STRING,
 },
 uid: {
   type: Sequelize.INTEGER,
 }
};

var balanceConfig = {
  freezeTableName: true,
  timestamps: false,
};

/**
 * Returns the schema model of the balance table
 *
 * @param sequelize
 *   a cache mysql connection
 *
 * @return schema
 */
function getModel(sequelize) {
  return sequelize.define(balance, balanceSchema, balanceConfig);
}

module.exports = {
  userDataList,
  getModel,
};

