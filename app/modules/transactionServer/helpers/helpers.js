'use strict';

var _           = require('lodash');
var querystring = require('querystring');
var logger      = require('../../../utilities/logger');
var dbService   = require('../../core/Database');

/**
 * Helper function collect()
 *
 * @param Store
 *   the store reference
 * @param storeProp
 *   the property exposed from the store object
 * @param Handler
 *   a reference to the Handler object
 * @param data
 *   the data to be send along the query
 * @param method
 *   the method to be used on the request
 * @param config
 *   a reference to the configuration object
 * @param apiName
 *   the endpoint path name from drupal
 * @param http
 *   a reference to the http module
 * @param socket
 *   a reference to the socket client object
 * @param socketEvent
 *   the name of the socket event to be emitted
 */
function collect(Store, storeProp, Handler, data, method, config, apiName, http, socket, socketEvent) {
  if (Store && Store[storeProp].length) {
    if (socket && socketEvent) {
      var found = _searchTheData(Store, storeProp, socket);

      if (found < 0) {
        logger('Cache data is not found for ' + socketEvent + ' data', 'warn');
        logger('Creating a new cache for it', 'info');
        _createNewRecord(Store, storeProp, Handler, data, method, config, apiName, http, socket, socketEvent);

        return;
      }

      logger('Retrieving data from cache ' + socketEvent + ' data', 'info');
      _emitEvent(socketEvent, socket, Store, storeProp, found);

      return;
    }
  }

  _createNewRecord(Store, storeProp, Handler, data, method, config, apiName, http, socket, socketEvent);
}

/**
 * Helper function for creating a new record from the Store cache
 *
 * @param Store
 *   the Store object
 * @param storeProp
 *   the exposed property name of the Store object
 * @param Handler
 *   the Class object Handler
 * @param data
 *   object containing data
 * @param method
 *   the request method type
 * @param config
 *   a reference to the config object
 * @param http
 *   a reference to the http object module
 * @param socket
 *   a reference to the returned socket object
 * @param socketEvent
 *   the name of the socket event to trigger
 */
function _createNewRecord(Store, storeProp, Handler, data, method, config, apiName, http, socket, socketEvent) {
  var dataQuery = data;
  var handler = undefined;
  const connection = dbService.getConnection();

  if (storeProp === 'userDataList') {
    var model = Store.getModel(connection);

    _getUserData(socket.request._query.uid, model).then(function(data) {
      _pushData(Store, storeProp, data, socket);

      if (socket && socketEvent) {
        _emitEvent(socketEvent, socket, Store, storeProp, Store[storeProp].length - 1);
        return;
      }
    });
  }
  else if (storeProp === 'messageList') {
    _getChatData(Store, connection).then((data) => {
      _pushData(Store, storeProp, data, socket);

      if (socket && socketEvent) {
        _emitEvent(socketEvent, socket, Store, storeProp, Store[storeProp].length - 1);
        return;
      }
    });
  }
  else {
    if (data && data.connections) {
      var found = _.findIndex(data.connections, { 'uid': socket.request._query.uid });
      dataQuery = querystring.stringify({ uid: data.connections[found].uid });
    }

    handler = new Handler(dataQuery, method, config, apiName, http);
    handler.execute(function(error, result) {
      logger('Retrieving data from ' + apiName, 'info');

      if (error) {
        logger('Something went wrong while retrieving data from ' + apiName, 'error');
        logger(error, 'debug');

        return;
      }

      _pushData(Store, storeProp, result.data, socket);

      if (socket && socketEvent) {
        _emitEvent(socketEvent, socket, Store, storeProp, Store[storeProp].length - 1);

        return;
      }
    });
  }
}

/**
 * Get the user balance directly from Drupal's database
 *
 * @param uid
 *   the unique user id of a Drupal user
 * @param model
 *   the sequelize model representation of the balance schema
 *
 * @return Promise Object
 */
function _getUserData(uid, model) {
  let data = {
    balances: {},
    transactions: {
      complete: '',
      recent: '',
      open_buy: '',
      open_sell: '',
    },
  };

  return new Promise((resolve, reject) => {
    dbService.query(model, { where: { uid } }).then(function(result) {
      if (!result.length) {
        reject();
      }

      let balance = {};

      for (var i = 0; i < result.length; i++) {
        let {
          amount__number,
          amount__currency_code
        } = result[i].dataValues;

        balance[amount__currency_code] = {
          amount: amount__number,
          formatted: `${amount__number} ${amount__currency_code}`
        };
      }

      data.balances = balance;
      resolve(data);
    });
  });
}

/**
 * Retrieve the chat history from Drupal database
 *
 * @param store
 *   the store object for chat
 * @param connection
 *   the cached connection object
 *
 * @return Promise Object
 */
function _getChatData(store, connection) {
  let chatHistory = [];
  const chatModel = store.getModel(connection);
  const userFieldDataModel = store.UserFieldDataStore.getModel(connection);

  return new Promise((resolve, reject) => {
    dbService.query(chatModel, {
      where: {
        visible: 1,
        uid: {
          $ne: null,
        }
      }})
      .then(async (result) => {
        if (!result.length) {
          reject();
        }

        for (let i = 0; i < result.length; i++) {
          const { created, uid, message__value } = result[i].dataValues;
          const user = await _getUserNamePerUID(uid, userFieldDataModel);

          chatHistory.push({
            time: _formatDate(created),
            message: message__value,
            user,
          });
        }

        resolve(chatHistory);
    });
  });
}

/**
 * Get the name of a user per uid
 *
 * @param uid
 *   the unique id of the user from Drupal
 * @param model
 *   the sequel model object
 *
 * @return Promise Object
 */
function _getUserNamePerUID(uid, model) {
  return new Promise((resolve, reject) => {
    dbService.query(model, {
      where: {
        uid,
      }})
      .then((result) => {
        if (result.length) {
          resolve(result[0].dataValues.name);
        }
        else {
          resolve('anonymous');
        }
      });
  });
}

/**
 * Format the give timestamp into date format e.g. m/d/Y
 *
 * @param timestamp
 *   the timestamp to convert into date format
 * @return date format
 */
function _formatDate(timestamp) {
  const date = new Date(timestamp * 1000);
  const month = (date.getMonth() + 1) > 9 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1);
  const d = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
  const year = date.getFullYear();
  const hours = date.getHours() > 9 ? date.getHours() : '0' + date.getHours();
  const minutes = date.getMinutes() > 9 ? date.getMinutes() : '0' + date.getMinutes();

  return `${month}/${d}/${year} ${hours}:${minutes}`;
}

/**
 * Helper function that execute the actual socket event
 *
 * @param socketEvent
 *   the name of the socketEvent to trigger
 * @param socket
 *   a reference to the socket object
 * @param Store
 *   the store cache object
 * @param storeProp
 *   the exposed property name of the Store object
 * @param found
 *   the index value from the Store object
 */
function _emitEvent(socketEvent, socket, Store, storeProp, found) {
  logger('Sending a burst for ' + socketEvent + ' event', 'info');

  if (storeProp === 'userDataList') {
    socket.emit(socketEvent, Store[storeProp][found].data);
  }
  else {
    socket.emit(socketEvent, Store[storeProp]);
  }
}

/**
 * Helper function to look for the data to be sent via socket event
 *
 * @param Store
 *   a reference to the Store cache object
 * @param storeProp
 *   the exposed property name of the Store cache object
 * @param socket
 *   a reference to the socket object
 *
 * @return found
 *   an integer value OR -1
 */
function _searchTheData(Store, storeProp, socket) {
  var found = -1;

  if (storeProp === 'userDataList') {
    found = _.findIndex(Store[storeProp], { 'uid': socket.request._query.uid });
  }
  else {
    found = Store[storeProp].length - 1;
  }

  return found;
}

/**
 * Helper function to push the data to the Store cache object
 *
 * @param Store
 *   a reference to the Store cache object
 * @param storeProp
 *   the exposed property name of the store cache object
 * @param data
 *   the data object containing data
 * @param socket
 *   a reference to the socket object
 */
function _pushData(Store, storeProp, data, socket) {
  if (storeProp === 'userDataList') {
    Store[storeProp].push({
      uid: socket.request._query.uid,
      data: data
    });
  }
  else if (storeProp === 'messageList') {
    Store[storeProp] = data;
  }
  else {
    Store[storeProp].push(data);
  }
}

/**
 * Run a chart data cron job in every 30 seconds
 *
 * @param cron
 *   a reference to the node-cron module
 * @param Handler
 *   a reference of the Handler object
 * @param method
 *   the method to use on the sending request
 * @param config
 *   the configuration object
 * @param apiName
 *   the name of the drupal endpoint api
 * @param http
 *   a reference to the http module
 * @param SocketStore
 *   a reference to the SocketStore object container
 * @return job | Promise Object
 */
function chartDataCronJob(cron, Handler, method, config, apiName, http, SocketStore) {
  var job = cron.schedule(config.host.cron.chartData, function() {
    logger('Run the job for Dashboard Chart Data update', 'info');

    var handler = new Handler(null, method, config, apiName, http);
    handler.execute(function(error, result) {
      logger('Retrieving data from ' + apiName, 'info');

      if (error) {
        logger('Something went wrong while retrieving data from ' + apiName, 'error');
        logger(error, 'debug');

        return;
      }

      if (SocketStore.connections.length) {
        SocketStore.connections.forEach(function(key, currentIndex) {
          logger('Sending updated data for dashboard chart', 'info');
          SocketStore.connections[currentIndex].socket.emit('chart_data', result.data);
        });

        return;
      }

      logger('SocketStore is not populated. There will be no socket event to trigger', 'warn');

      return;
    });
  });

  return Promise.resolve(job);
}

function altCoinsDataCronJob(cron, Handler, method, config, apiName, http, SocketStore) {
  var job = cron.schedule(config.host.cron.altCoinsData, function() {
    logger('Run the job for Altcoins Data update', 'info');

    var handler = new Handler(null, method, config, apiName, http);
    handler.execute(function(error, result) {
      logger('Retrieving data from ' + apiName, 'info');

      if (error) {
        logger('Something went wrong while retrieving data from ' + apiName, 'error');
        logger(error, 'debug');

        return;
      }

      if (SocketStore.connections.length) {
        SocketStore.connections.forEach(function(key, currentIndex) {
          logger('Sending updated data for Altcoins', 'info');
          SocketStore.connections[currentIndex].socket.emit('altcoins_data', result.data);
        });

        return;
      }

      logger('SocketStore is not populated. There will be no socket event to trigger', 'warn');

      return;
    });
  });

  return Promise.resolve(job);
}

module.exports = {
  collect             : collect,
  chartDataCronJob    : chartDataCronJob,
  altCoinsDataCronJob : altCoinsDataCronJob
};

