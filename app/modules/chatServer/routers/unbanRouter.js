'use strict';

var router = require('express').Router();
var unban  = require('../handlers/unban');

router.post('/', unban.unban);

module.exports = router;

