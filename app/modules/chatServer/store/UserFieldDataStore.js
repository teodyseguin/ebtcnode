'use strict';

var Sequelize = require('sequelize');

var table = 'users_field_data';
var schema = {
  uid: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  langcode: {
    type: Sequelize.STRING,
  },
  preferred_langcode: {
    type: Sequelize.STRING,
  },
  preferred_admin_langcode: {
    type: Sequelize.STRING,
  },
  name: {
    type: Sequelize.STRING,
  },
  pass: {
    type: Sequelize.STRING,
  }
};

var config = {
  freezeTableName: true,
  timestamps: false,
};

function getModel(sequelize) {
  return sequelize.define(table, schema, config);
}

module.exports = {
  getModel,
}
