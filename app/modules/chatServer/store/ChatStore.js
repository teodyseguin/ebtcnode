var banList     = [];
var messageList = [];
var UserFieldDataStore = require('../store/UserFieldDataStore');
var Sequelize = require('sequelize');

var chatHistory = 'ezbtc_chat_log';

var chatHistorySchema = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  type: {
    type: Sequelize.STRING,
  },
  uuid: {
    type: Sequelize.STRING,
  },
  uid: {
    type: Sequelize.INTEGER,
  },
  created: {
    type: Sequelize.INTEGER,
  },
  visible: {
    type: Sequelize.INTEGER,
  },
  ip_address: {
    type: Sequelize.STRING,
  },
  message__value: {
    type: Sequelize.STRING,
  },
  message__format: {
    type: Sequelize.STRING,
  }
};

var chatHistoryConfig = {
  freezeTableName: true,
  timestamps: false,
};

function getModel(sequelize) {
  return sequelize.define(chatHistory, chatHistorySchema, chatHistoryConfig);
}

module.exports = {
  banList,
  messageList,
  getModel,
  UserFieldDataStore,
};

