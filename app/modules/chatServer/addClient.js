'use strict';

var _           = require('lodash');
var ban         = require('./handlers/ban');
var config      = require('../../config')[process.env.NODE_ENV || 'dev'];
var http        = require('http');
var logger      = require('../../utilities/logger');
var message     = require('./handlers/message');
var querystring = require('querystring');
var ChatStore   = require('./store/ChatStore');
var SocketStore = require('./store/SocketStore');
var Handler     = require('../core/Handler');;

/**
 * Adds a client to the chat server
 *
 * @param io
 *   the societ.io instance
 * @param socket
 *   a returned argument of a callback upon connection
 *   e.g. io.on('connection, function(socket){})
 */
function addClient(io, socket) {
  socket.on('chat_send', function(data) {
    if (checkBanList(data, ChatStore.banList)) {
      socket.emit('chat_receive', { message: 'You are currently unable to post on chat', time: new Date() });
      return;
    }

    io.emit('chat_receive', data);

    var dataWithIp = addIpAddress(data, socket);
    var messageData = querystring.stringify(dataWithIp);
    var handler = new Handler(messageData, 'POST', config, 'chatLog', http);
    handler.execute(function(error, result) {
      if (error) {
        logger('Something went wrong', 'error');
        logger(error, 'debug');
      }

      ChatStore.messageList.push(dataWithIp);
    });
  });

  socket.on('disconnect', function() {
    logger('Socket disconnected from ip address: ' + socket.request.connection.remoteAddress + ' with uid: ' + socket.request._query.uid, 'info');

    var found = _.findIndex(SocketStore.connections, { 'uid': socket.request._query.uid });

    SocketStore.connections.splice(found, 1);
  });
}

/**
 * Find out if the `uid` from the message
 * object is withi the ban list
 *
 * @param data
 *   the message object
 * @param banList
 *   the list of bans in a form of array
 * @return boolean
 */
function checkBanList(data, banList) {
  if (banList.indexOf(data.uid) > -1) {
    return true;
  }

  return false;
}

/**
 * Function to ensure that the message object
 * contains an ip address property
 *
 * @param data
 *   the message object
 * @param socket
 *   an instance of socket io
 * @return ensured message object with ip_address
 */
function addIpAddress(data, socket) {
  if (!data.hasOwnProperty('ip_address')) {
    Object.defineProperty(
      data, 'ip_address', { value: socket.request.connection.remoteAddress, writable: true, enumerable: true, configurable: true }
    );

    return data;
  }

  return data;
}

module.exports = addClient;

