'use strict';

var config        = require('../../../config')[process.env.NODE_ENV || 'dev'];
var http          = require('http');
var logger        = require('../../../utilities/logger');
var printResponse = require('../../../utilities/printResponse');
var querystring   = require('querystring');
var Handler       = require('../../core/Handler');;

/**
 * Response handler for when someone want's to send a message
 */
function messages(request, response, callback) {
  if (!request.body.params.hasOwnProperty('ip_address')) {
    Object.defineProperty(
      request.body.params,
      'ip_address',
      {
        value: request.header('x-forwarded-for') || request.connection.remoteAddress
      }
    );
  }

  var messageData = querystring.stringify(request.body.params);
  var handler = new Handler(messageData, 'POST', config, 'chatLog', http);
  handler.execute(function(error, result) {
    if (typeof callback === 'function') {
      return callback(printResponse(error, response, result)); 
    } 
  });
}

module.exports = {
  messages : messages 
};

