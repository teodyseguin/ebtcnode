'use strict';

var ChatStore     = require('../store/ChatStore');
var config        = require('../../../config')[process.env.NODE_ENV || 'dev'];
var http          = require('http');
var logger        = require('../../../utilities/logger');
var printResponse = require('../../../utilities/printResponse');
var Handler       = require('../../core/Handler');
var querystring   = require('querystring');

/**
 * Response handler for when someone want's to send a ban
 */
function bans(request, response, callback) {
  if (request.method === 'POST') {
    var sheIsInThere = ChatStore.banList.indexOf(request.body.params.uid);
    response.end();

    if (sheIsInThere < 0) {
      ChatStore.banList.push(request.body.params.uid);
      logger('User ' + request.body.params.uid + ' was added to ChatStore.banList', 'info'); 
    }
  }
  else {
    var banData = querystring.stringify(request.body.params);
    var handler = new Handler(banData, 'GET', config, 'chatBan', http);
    handler.execute(function(error, result) {
      if (typeof callback === 'function') {
        return callback(printResponse(error, response, result)); 
      }
    });
  }
}

module.exports = {
  bans: bans
};

