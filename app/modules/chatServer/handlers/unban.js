'use strict';

var ChatStore = require('../store/ChatStore');
var logger    = require('../../../utilities/logger');

/**
 * Response handler for when someone want's to unban a user
 */
function unban(request, response) {
  if (request.method === 'POST') {
    var found = ChatStore.banList.indexOf(request.body.params.uid);
    response.end();

    if (found > -1) {
      ChatStore.banList.splice(found, 1);
      logger('User ' + request.body.params.uid + ' has been removed from ChatStore.banList', 'info');
    } 
  }
}

module.exports = {
  unban: unban
};

