'use strict';

var addClient      = require('./addClient');

var banRouter      = require('./routers/banRouter');
var messageRouter  = require('./routers/messageRouter');
var unbanRouter    = require('./routers/unbanRouter');

module.exports = {
  addClient     : addClient,
  banRouter     : banRouter,
  messageRouter : messageRouter,
  unbanRouter   : unbanRouter
};

