'use strict';

var Bitstamp = require('bitstamp-ws');
var SocketStore = require('../chatServer/store/SocketStore.js');
var logger = require('../../utilities/logger');

var intervalDone = true;
var intervalCounter = 30;
var interval = undefined;

/**
 * Get the best price from live order book, from bitstamp
 */
function getBestPrice() {
  var ws = new Bitstamp();

  ws.on('data', function(data) {
    if (intervalDone) {
      clearInterval(interval);

      if (_checkValid(data.bids[0])) {
        intervalDone = false;
        _notifyConnectedSockets(data.bids[0]);
      }
    }
  }); 
}

/**
 * Check if bids is not undefined
 *
 * @param bids
 *   the bid object
 * 
 * @return boolean
 *   true | false
 */
function _checkValid(bids) {
  if (bids) {
    return true;
  }
  else {
    return false;
  }
}

/**
 * Emit events to connected sockets
 *
 * @param bids
 *   the bid object
 */
function _notifyConnectedSockets(bids) {
  interval = setInterval(function() {
    _sendEventCounter(intervalCounter);
    intervalCounter--;

    if (intervalCounter === -1) {
      _sendBitstampEvent(bids);

      intervalDone = true;
      intervalCounter = 30;
    }
  }, 1000);
}

/**
 * Wrapper function to send bitstamp events to connected sockets
 *
 * @param bids
 *   the bid information from bitstamp
 */
function _sendBitstampEvent(bids) {
  if (SocketStore.connections.length) {
    SocketStore.connections.forEach(function(key, currentIndex) {
      SocketStore.connections[currentIndex].socket.emit('bitstamp_best_price', bids);
    }); 
  }
}

/**
 * Wrapper function to send counter events to connected sockets
 *
 * @param counter
 */
function _sendEventCounter(counter) {
  if (SocketStore.connections.length) {
    SocketStore.connections.forEach(function(key, currentIndex) {
      SocketStore.connections[currentIndex].socket.emit('counter', counter);
    }); 
  }
}

module.exports = getBestPrice;

