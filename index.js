var _                      = require('lodash');
var app                    = require('express')();
var cron                   = require('node-cron');
var bodyParser             = require('body-parser');
var chatServer             = require('./app/modules/chatServer');
var transactionServer      = require('./app/modules/transactionServer');
var config                 = require('./app/config')['dev'];
var http                   = require('http').Server(app);
var http2                  = require('http');
var io                     = require('socket.io')(http);
var logger                 = require('./app/utilities/logger');
var ChatStore              = require('./app/modules/chatServer/store/ChatStore');
var Handler                = require('./app/modules/core/Handler');
var SocketStore            = require('./app/modules/chatServer/store/SocketStore');
var TickerInformationStore = require('./app/modules/transactionServer/store/TickerInformationStore');
var TransactionStore       = require('./app/modules/transactionServer/store/TransactionStore');
var UserDataStore          = require('./app/modules/transactionServer/store/UserDataStore');
var querystring            = require('querystring');
var routes                 = require('./routes').routes;
var bitstamp               = require('./app/modules/bitstamp');
var Worker                 = require('webworker-threads').Worker;
var dbService              = require('./app/modules/core/Database');

// Simple port connection
http.listen(3000, function() {
  logger('Listening on *: 3000', 'info');

  // chartDataCronJob returns a promise job which can be
  // use later, if needed to extend this feature and have
  // the cron job to stop. use *.stop() method
  var chartCronJob = transactionServer
      .helpers
      .chartDataCronJob(
        cron,
        Handler,
        'GET',
        config,
        'chartData',
        http2,
        SocketStore
      );

  var altCoinsCronJob = transactionServer
      .helpers
      .altCoinsDataCronJob(
        cron,
        Handler,
        'GET',
        config,
        'altCoinsData',
        http2,
        SocketStore
      );

  var options = {
    host: config.mysql.host,
    database: config.mysql.database,
    username: config.mysql.username,
    password: config.mysql.password,
    port: config.mysql.port,
  };

  var connection = dbService.connect(options);
  connection.authenticate().then(function() {
    logger('Connected successfully to MySQL', 'info');
  })
  .catch(function(err) {
    logger('Something went wrong while connecting to MySQL', 'error');
    logger(err, 'debug');
  });
});

// Socket IO
io.use(function(socket, next) {
  var found = _.findIndex(SocketStore.connections, { 'uid': socket.request._query.uid });

  if (found < 0) {
    SocketStore.connections.push({
      uid: socket.request._query.uid,
      socket: socket
    });

    logger('Number of connected socket clients atm: ' + SocketStore.connections.length, 'info');
  }

  next();
});

io.on('connection', function(socket) {
  if (socket.hasOwnProperty('id') && socket.id) {
    var helpers = transactionServer.helpers;

    logger('User connected from ip address: ' + socket.request.connection.remoteAddress + ' with uid: ' + socket.request._query.uid, 'info');

    socket.on('request_data', function(data) {
      var socketUID = socket.request._query.uid;

      logger('Data burst request from ip address: ' + socket.request.connection.remoteAddress + ' with uid: ' + socket.request._query.uid, 'info');

      if (socketUID > 0) {
        helpers.collect(
          ChatStore,
          'banList',
          Handler,
          null,
          'GET',
          config,
          'chatBan',
          http2,
          null
        );

        helpers.collect(
          TickerInformationStore,
         'tickerInformationList',
         Handler,
         null,
         'GET',
         config,
         'marketTrends',
         http2,
         socket,
         'market_retrieved');

        helpers.collect(
          ChatStore,
          'messageList',
          Handler,
          null,
          'GET',
          config,
          'chatLog',
          http2,
          socket,
          'messages_retrieved'
        );

        helpers.collect(
          TransactionStore,
          'transactionList',
          Handler,
          null,
          'GET',
          config,
          'transactions',
          http2,
          socket,
          'transactions_retrieved'
        );

        helpers.collect(
          UserDataStore,
          'userDataList',
          Handler,
          SocketStore,
          'POST',
          config,
          'userData',
          http2,
          socket,
          'user_data'
        );
      }
      else {
        helpers.collect(
          TickerInformationStore,
          'tickerInformationList',
          Handler,
          null,
          'GET',
          config,
          'marketTrends',
          http2,
          socket,
          'market_retrieved'
        );
      }
    });

    // Add client to the chat server.
    chatServer.addClient(io, socket);
  }
});

// initiate all routes
routes(app);

// initiate a worker for bitstamp get best price
var worker = new Worker(bitstamp.getBestPrice());

