'use strict';

var bodyParser = require('body-parser');
var chatServer = require('./app/modules/chatServer');
var transactionServer = require('./app/modules/transactionServer');

module.exports.routes = function(app) {
  // ensure request body can be parsed
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());

  // Routes and Endpoints
  app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
  });

  // API Endpoint for sending message
  app.use('/v1/chat/messages', chatServer.messageRouter);
  // API Endpoint for retrieving(GET) user bans and sending(POST) new user bans
  app.use('/v1/chat/bans', chatServer.banRouter);
  // API Endpoint for unban(POST)
  app.use('/v1/chat/unban', chatServer.unbanRouter);
  // API Endpoint for transaction(POST)
  app.use('/v1/transactions', transactionServer.transactionRouter);
}

