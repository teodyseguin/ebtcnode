'use strict';

var gulp    = require('gulp');
var plugins = require('gulp-load-plugins')({lazy: true});

gulp.task('test', function() {
  return gulp
    .src('./test/**/*.spec.js', {read: false})
    .pipe(plugins.mocha({report: 'spec'}))
    .pipe(plugins.istanbul.writeReports())
    .on('error', function(err) {
      throw err;
    })
    .on('end', function() {
      process.exit();
    });
});

gulp.task('default', ['test']);

